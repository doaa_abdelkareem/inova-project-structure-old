package com.example.inovastructureold.models

import com.example.inovastructureold.data.local.entities.MovieEntity

data class Movie(
    val id: Int? = null,
    val posterPath: String? = null,
    val backdropPath: String? = null,
    val title: String? = null,
    val popularity: Double? = null,
    val genres: String? = null,
    val originalLanguage: String? = null,
    val overview: String? = null,
    val releaseDate: String? = null,
    val revenue: Int? = null,
    val originalTitle: String? = null
)

fun Movie.asMovieDatabaseModel(): MovieEntity {
    return MovieEntity(
        id, posterPath, backdropPath, title, popularity, genres,
        originalLanguage, overview, releaseDate, (this as Movie).revenue, originalTitle
    )
}

fun List<Movie>.asMovieDatabaseModel(): List<MovieEntity> {
    return map {
        MovieEntity(
            it.id,
            it.posterPath,
            it.backdropPath,
            it.title,
            it.popularity,
            it.genres,
            it.originalLanguage,
            it.overview,
            it.releaseDate,
            (it as Movie).revenue,
            it.originalTitle
        )
    }
}