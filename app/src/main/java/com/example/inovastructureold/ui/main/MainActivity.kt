package com.example.inovastructureold.ui.main

import android.os.Bundle
import com.example.inovastructureold.R
import com.example.inovastructureold.core.base.BaseActivity
import com.example.inovastructureold.core.utils.FragmentManagerUtil
import com.example.inovastructureold.ui.movies.MoviesFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.main_toolbar.view.*

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()

    }

    fun initViews() {
        toolbar_main.image_view_back.setOnClickListener { onBackPressed() }

        FragmentManagerUtil.replaceFragment(supportFragmentManager,
            MoviesFragment.newInstance(),
            R.id.placeholder)
    }

}