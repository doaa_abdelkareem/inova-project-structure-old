package com.example.inovastructureold.ui.moviedetails

import android.view.View
import androidx.fragment.app.viewModels
import com.example.inovastructureold.R
import com.example.inovastructureold.core.base.BaseActivity
import com.example.inovastructureold.core.base.BaseFragment
import com.example.inovastructureold.models.Movie
import kotlinx.android.synthetic.main.fragment_movie_details.*
import timber.log.Timber

class MovieDetailsFragment :
    BaseFragment<Movie, MovieDetailsViewModel>(R.layout.fragment_movie_details) {

    companion object {
        @JvmStatic
        fun newInstance() =
            MovieDetailsFragment()

    }

    override val viewModel: MovieDetailsViewModel by viewModels()

    override fun initViews(parentView: View?) {
        super.initViews(parentView)

        (requireActivity() as BaseActivity).changeToolbarTitle(getString(R.string.movie_details))
        initSwipeToRefreshListener(swipeRefreshLayoutMovieDetails)

    }

    override fun refreshData() {
        super.refreshData()

        Timber.d("Refresh done!")
        swipeRefreshLayout?.isRefreshing = false
    }

}
