package com.example.inovastructureold.ui.movies

import android.view.View
import com.example.inovastructureold.R
import com.example.inovastructureold.core.base.BasePaginationAdapter
import com.example.inovastructureold.core.base.BaseViewHolder
import com.example.inovastructureold.models.Movie

class MoviesAdapter : BasePaginationAdapter<Movie>(R.layout.item_movie) {

    /* TODO you can even eliminate the need to always override this method by providing something like
         a factory for view holders' views in base class since only layout and view holder types change */
    // override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
    //    return if (viewType == TYPE_LOADING_FOOTER)
    //    getLoadingViewHolder(parent)
    //    else {
    //        val view =
    //            LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
    //        return MoviesViewHolder(view, parent.context).apply {
    //            onItemClickListener.subscribe(
    //                this@MoviesAdapter.onItemClickListener
    //            )
    //        }
    //    }
    //    }
    override fun makeViewHolder(view: View): BaseViewHolder<Movie> = MoviesViewHolder(view, view.context)
}
