package com.example.inovastructureold.ui.movies

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.example.inovastructureold.R
import com.example.inovastructureold.core.base.BaseActivity
import com.example.inovastructureold.core.base.BasePaginationFragment
import com.example.inovastructureold.core.enums.ItemClickStatus
import com.example.inovastructureold.core.utils.FragmentManagerUtil
import com.example.inovastructureold.models.Movie
import com.example.inovastructureold.ui.moviedetails.MovieDetailsFragment
import kotlinx.android.synthetic.main.fragment_movies.*
import timber.log.Timber


class MoviesFragment : BasePaginationFragment<Movie, MoviesViewModel>(R.layout.fragment_movies) {

    companion object {
        @JvmStatic
        fun newInstance() =
            MoviesFragment()

    }

    override val viewModel: MoviesViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        doRequest()

    }

    override fun initViews(parentView: View?) {
        super.initViews(parentView)

        (requireActivity() as BaseActivity).changeToolbarTitle(getString(R.string.popular_movies))

        initPagination(
            recycler_view_movies, GridLayoutManager(mContext, 3),
            MoviesAdapter().apply {
                bag.add(this.onItemClickListener.subscribe {
                    when (it.second) {
                        ItemClickStatus.OPEN -> FragmentManagerUtil.replaceFragment(
                            requireActivity().supportFragmentManager,
                            MovieDetailsFragment.newInstance(),
                            R.id.placeholder
                        )
                        ItemClickStatus.EDIT -> Timber.d("Editing...")
                    }
                })

            }
        )

        initSwipeToRefreshListener(swipeRefreshLayoutMovies)
    }

    override fun loadPage() {
        super.loadPage()
        text_view_empty_movies_list.visibility = View.GONE

        viewModel.getPopularMovies(pageNumber)
            .subscribe(getObserver())
    }

    override fun noData() {
        text_view_empty_movies_list.visibility = View.VISIBLE
    }

    override fun loadEmptyPlaceholder() {
    }

}