package com.example.inovastructureold.ui.movies

import androidx.lifecycle.ViewModel
import com.example.inovastructureold.MoviesApp
import com.example.inovastructureold.models.Movie
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

class MoviesViewModel : ViewModel() {
    private val moviesRepository = MoviesApp.appContainer.moviesRepository

    fun getPopularMovies(
        pageNumber: Int
    ): Single<List<Movie>> {
        return moviesRepository
            .getPopularMovies(pageNumber)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }
}