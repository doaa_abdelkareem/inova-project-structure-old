package com.example.inovastructureold.ui.movies

import android.content.Context
import android.view.View
import com.bumptech.glide.Glide
import com.example.inovastructureold.core.base.BaseViewHolder
import com.example.inovastructureold.core.enums.ItemClickStatus
import com.example.inovastructureold.models.Movie
import com.jakewharton.rxbinding4.view.clicks
import kotlinx.android.synthetic.main.item_movie.view.*
import java.util.concurrent.TimeUnit


class MoviesViewHolder(view: View, private val context: Context) :
    BaseViewHolder<Movie>(view) {

    lateinit var movie: Movie

    init {
        itemView.clicks().throttleFirst(1500, TimeUnit.MILLISECONDS).map {
            Pair(
                movie,
                ItemClickStatus.OPEN
            )
        }.subscribe(onItemClickListener)
    }

    override fun bind(item: Movie) {
        movie = item

        Glide
            .with(context)
            .load("https://image.tmdb.org/t/p/original/"+item.posterPath)
            .centerCrop()
//            .placeholder(R.drawable.loading_animation)
//            .error(R.drawable.ic_broken_image)
            .into(itemView.image_view_video_poster)

        itemView.text_view_title.text = item.title
        itemView.text_view_popularity.text  = item.popularity.toString()
    }
}