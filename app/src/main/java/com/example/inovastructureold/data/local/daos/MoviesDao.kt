package com.example.inovastructureold.data.local.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.inovastructureold.data.local.entities.MovieEntity
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

@Dao
interface MoviesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun cachePopularMovies(movieEntities: List<MovieEntity>): Completable

    @Query("SELECT * FROM movie_table")
    fun getPopularMovies(): Single<List<MovieEntity>>
}