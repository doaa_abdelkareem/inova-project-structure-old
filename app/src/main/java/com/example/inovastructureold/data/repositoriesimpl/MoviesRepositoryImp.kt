package com.example.inovastructureold.data.repositoriesimpl

import com.example.inovastructureold.core.base.BaseRepo
import com.example.inovastructureold.core.utils.NetworkHandler
import com.example.inovastructureold.domain.repositories.MoviesLocalDataSource
import com.example.inovastructureold.domain.repositories.MoviesRemoteDataSource
import com.example.inovastructureold.domain.repositories.MoviesRepository
import com.example.inovastructureold.models.Movie
import io.reactivex.rxjava3.core.Single

/* TODO this is a way to eliminate some boilerplate code */
class MoviesRepositoryImp(
    private val moviesRemoteDataSource: MoviesRemoteDataSource,
    private val moviesLocalDataSource: MoviesLocalDataSource,
    networkHandler: NetworkHandler
) : BaseRepo(networkHandler), MoviesRepository {
    override fun getPopularMovies(page: Int): Single<List<Movie>> {
        return fetch({ moviesRemoteDataSource.getPopularMovies(page) },
            { moviesLocalDataSource.getPopularMovies(page) },
            { moviesLocalDataSource.cachePopularMovies(it) })
    }
}