package com.example.inovastructureold.data.remote.api

import com.example.inovastructureold.data.remote.responses.MoviesResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesAPI {

    @GET(API_POPULAR_MOVIES)
    fun getPopularMovies(@Query(QUERY_PARAMETER_PAGE) page: Int): Single<MoviesResponse>
}