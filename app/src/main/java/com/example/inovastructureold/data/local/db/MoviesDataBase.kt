package com.example.inovastructureold.data.local.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.inovastructureold.data.local.daos.MoviesDao
import com.example.inovastructureold.data.local.entities.MovieEntity

@Database(
    entities = [MovieEntity::class], version = 1, exportSchema = true
)
abstract class MoviesDataBase : RoomDatabase() {

    companion object {
        private const val DB_NAME = "movies_db"

        @Volatile
        private var instance: MoviesDataBase? = null

        fun getInstance(context: Context): MoviesDataBase = instance ?: synchronized(this) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, MoviesDataBase::class.java, DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }

    abstract fun moviesDao(): MoviesDao
}