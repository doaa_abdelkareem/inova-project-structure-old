package com.example.inovastructureold.data.remote.remotedatasourse

import com.example.inovastructureold.data.remote.api.MoviesAPI
import com.example.inovastructureold.data.remote.responses.asDomainModel
import com.example.inovastructureold.domain.repositories.MoviesRemoteDataSource
import com.example.inovastructureold.models.Movie
import io.reactivex.rxjava3.core.Single


class MoviesRemoteDataSourceImp(private val moviesAPI: MoviesAPI) : MoviesRemoteDataSource {
    override fun getPopularMovies(page: Int): Single<List<Movie>> {
        return moviesAPI.getPopularMovies(page)
            .map {
                it.asDomainModel()
            }
    }

}
