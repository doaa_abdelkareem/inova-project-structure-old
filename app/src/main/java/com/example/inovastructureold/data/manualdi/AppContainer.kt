package com.example.inovastructureold.data.manualdi

import com.example.inovastructureold.MoviesApp
import com.example.inovastructureold.core.utils.NetworkHandlerImpl
import com.example.inovastructureold.data.local.db.MoviesDataBase
import com.example.inovastructureold.data.local.localdatasource.MoviesLocalDataSourceImp
import com.example.inovastructureold.data.remote.api.MoviesAPI
import com.example.inovastructureold.data.remote.api.MoviesApiManager
import com.example.inovastructureold.data.remote.remotedatasourse.MoviesRemoteDataSourceImp
import com.example.inovastructureold.data.repositoriesimpl.MoviesRepositoryImp

class AppContainer {

    val moviesRepository by lazy {
        MoviesRepositoryImp(
            moviesRemoteDataSource,
            moviesLocalDataSource,
            networkHandler
        )
    }

    private val moviesRemoteDataSource by lazy {
        MoviesRemoteDataSourceImp(moviesApi)
    }

    private val moviesLocalDataSource by lazy {
        MoviesLocalDataSourceImp(
            moviesDao
        )
    }

    private val moviesApi by lazy { MoviesApiManager.getApi(MoviesAPI::class.java)}
    private val moviesDatabase by lazy { MoviesDataBase.getInstance(MoviesApp.context) }
    private val moviesDao by lazy { moviesDatabase.moviesDao() }
    private val networkHandler by lazy { NetworkHandlerImpl() }

}