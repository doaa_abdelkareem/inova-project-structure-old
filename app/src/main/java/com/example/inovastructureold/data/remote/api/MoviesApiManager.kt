package com.example.inovastructureold.data.remote.api

import android.util.Log
import com.example.inovastructureold.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val TIME_OUT = 2L

class MoviesApiManager {

    companion object {

        @Volatile
        private var instance: Retrofit? = null

        fun <T> getApi(clazz: Class<T>): T {
            return getInstance().create(clazz)
        }

        private fun getInstance(): Retrofit = instance ?: synchronized(this) {
            instance ?: buildRetrofit()
                .also { instance = it }
        }

        private fun buildRetrofit() = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(gsonConvertFactory())
            .client(okHttpClient())
            .build()

        fun gsonConvertFactory() = GsonConverterFactory.create()

        private fun okHttpClient(): OkHttpClient {
            return OkHttpClient.Builder().apply {
                if (BuildConfig.DEBUG) {
                    addInterceptor(loggingInterceptor())
                }
                addInterceptor(authInterceptor())
                connectTimeout(TIME_OUT, TimeUnit.MINUTES)
                readTimeout(TIME_OUT, TimeUnit.MINUTES)
                writeTimeout(TIME_OUT, TimeUnit.MINUTES)
                retryOnConnectionFailure(true)
            }.build()
        }

        private fun loggingInterceptor() = HttpLoggingInterceptor { message ->
            Log.e(
                "OkHttp",
                message
            )
        }.setLevel(HttpLoggingInterceptor.Level.BODY)

        private fun authInterceptor() = Interceptor { chain ->
            chain.request().let {
                val newRequest = it.newBuilder()
                newRequest.url(
                    it.url.newBuilder()
                        .addQueryParameter(QUERY_PARAM_API_KEY, API_KEY)
                        .build()
                )
                chain.proceed(newRequest.build())
            }
        }

        private fun langInterceptor() = Interceptor { chain ->
            chain.request().let {
                val newRequest = it.newBuilder()
                newRequest.url(
                    it.url.newBuilder()
                        .addQueryParameter(QUERY_PARAM_LANGUAGE, LANGUAGE)
                        .build()
                )
                chain.proceed(newRequest.build())
            }
        }
    }
}