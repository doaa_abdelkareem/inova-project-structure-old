package com.example.inovastructureold.data.local.localdatasource

import com.example.inovastructureold.data.local.daos.MoviesDao
import com.example.inovastructureold.data.local.entities.asDomainModel
import com.example.inovastructureold.domain.repositories.MoviesLocalDataSource
import com.example.inovastructureold.models.Movie
import com.example.inovastructureold.models.asMovieDatabaseModel
import io.reactivex.rxjava3.core.Single


class MoviesLocalDataSourceImp(private val moviesDao: MoviesDao) : MoviesLocalDataSource {
    override fun getPopularMovies(page: Int): Single<List<Movie>> {
        return moviesDao.getPopularMovies()
            .map {
                it.asDomainModel()
            }
    }

    override fun cachePopularMovies(movies: List<Movie>) {
        moviesDao.cachePopularMovies(movies.asMovieDatabaseModel())
    }

}
