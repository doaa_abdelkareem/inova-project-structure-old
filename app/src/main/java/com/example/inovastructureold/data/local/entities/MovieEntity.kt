package com.example.inovastructureold.data.local.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.inovastructureold.models.Movie

@Entity(tableName = "movie_table")
data class MovieEntity(
    @PrimaryKey
    val id: Int? = null,
    val posterPath: String? = null,
    val backdropPath: String? = null,
    val title: String? = null,
    val popularity: Double? = null,
    val genres: String? = null,
    val originalLanguage: String? = null,
    val overview: String? = null,
    val releaseDate: String? = null,
    val revenue: Int? = null,
    val originalTitle: String? = null
)

fun MovieEntity.asDomainModel(): Movie {
    return Movie(
        id, posterPath, backdropPath, title, popularity, genres,
        originalLanguage, overview, releaseDate, this.revenue, originalTitle
    )
}

fun List<MovieEntity>.asDomainModel() =
    map {
        Movie(
            it.id, it.posterPath, it.backdropPath, it.title, it.popularity, it.genres,
            it.originalLanguage, it.overview, it.releaseDate, it.revenue, it.originalTitle
        )
    }