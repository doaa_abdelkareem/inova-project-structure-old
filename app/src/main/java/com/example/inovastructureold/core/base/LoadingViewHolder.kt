package com.example.inovastructureold.core.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.loader.view.*

class LoadingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun setProgressBarVisibility(isVisible: Boolean) {
        if (isVisible)
            itemView.progress_bar_load_more.visibility = View.VISIBLE
        else
            itemView.progress_bar_load_more.visibility = View.GONE

    }
}