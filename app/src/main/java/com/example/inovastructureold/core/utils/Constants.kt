package com.example.inovastructureold.core.utils

object Constants {
    const val PAGE_SIZE = 10
    const val REQUEST_SINGLE = "request_single"

    // TODO all capitals in constant vals
    const val App_PREFERENCES = "movies_prefs"
}