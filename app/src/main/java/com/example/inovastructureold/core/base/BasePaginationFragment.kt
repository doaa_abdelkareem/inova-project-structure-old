package com.example.inovastructureold.core.base

import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.inovastructureold.core.utils.Constants

/* TODO difference between this and lakom's structure in some bases (specially here in BaseFragment)
    is huge so I will wait for this part's completion before making comments there
 */
abstract class BasePaginationFragment<T, VM : ViewModel>(layoutId: Int) :
    BaseFragment<List<T>, VM>(layoutId) {

    private var pastVisibleItems = 0
    private var visibleItemCount = 0
    private var totalItemCount = 0
    private var isInitialized = false
    private var requestInProgress = false
    private var loadMore = true

    private lateinit var basePaginationAdapter: BasePaginationAdapter<T>
    private lateinit var recyclerView: RecyclerView
    protected var pageNumber = 1
    protected var extraDataAvailable = true
    protected var arrayList: ArrayList<T> = arrayListOf()

    override fun doRequest(type: String) {
        super.doRequest(type)
        if (requestInProgress) {
            promptRequestInProgress()
            return
        } else if (!extraDataAvailable) {
            loadMore = false
            promptNoMoreData()
            return
        } else if (!loadMore) {
            return
        }

        loadPage()
    }


    override fun onRequestCompleted() {
        requestInProgress = false

        swipeRefreshLayout?.isRefreshing = false

        basePaginationAdapter.setLoadingProgressBarVisibility(false)
    }

    override fun onRequestSucceeded(item: List<T>) {
        //add new items to the original list
        if (item.isNotEmpty()) {
            arrayList.addAll(item)
            basePaginationAdapter.updateDataSet(arrayList)
        } else {
            arrayList = ArrayList()
            basePaginationAdapter.updateDataSet(arrayList)
        }

        //if we got all the list from the api and there is no more items then we stop paging
        if (item.size < Constants.PAGE_SIZE) {
            extraDataAvailable = false
        }

        pageNumber++

        //if there is no data display empty list layout
        if (arrayList.isEmpty())
            noData()
    }

    override fun onRequestFailed(t: Throwable) {
        super.onRequestFailed(t)
        if (arrayList.isEmpty())
            loadEmptyPlaceholder()
        loadMore = false
    }

    private fun promptRequestInProgress() {
        recyclerView.post {
            basePaginationAdapter.setLoadingProgressBarVisibility(true)
        }
    }

    private fun promptNoMoreData() {
    }


    protected open fun loadPage() {
        requestInProgress = true
    }

    protected fun initPagination(
        recyclerView: RecyclerView,
        manager: RecyclerView.LayoutManager,
        baseAdapter: BasePaginationAdapter<T>,
        stopPagination: Boolean = false
    ) {
        this.recyclerView = recyclerView
        this.basePaginationAdapter = baseAdapter

        recyclerView.apply {
            adapter = baseAdapter
            layoutManager = manager
        }

        if (!stopPagination) {
            if (manager is LinearLayoutManager)
                setPaginationOnSwipe(recyclerView, manager)
            else if (manager is GridLayoutManager)
                setPaginationOnSwipe(recyclerView, manager)
        }

        isInitialized = true
    }

    private fun setPaginationOnSwipe(
        recyclerView: RecyclerView,
        layoutManager: LinearLayoutManager
    ) {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    if (loadMore) {
                        visibleItemCount = layoutManager.childCount
                        totalItemCount = layoutManager.itemCount
                        pastVisibleItems = layoutManager.findFirstVisibleItemPosition()
                        if (visibleItemCount + pastVisibleItems >= totalItemCount)
                            doRequest()
                    }
                }
            }
        })
    }

    private fun setPaginationOnSwipe(
        recyclerView: RecyclerView,
        layoutManager: GridLayoutManager
    ) {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    if (loadMore) {
                        visibleItemCount = layoutManager.childCount
                        totalItemCount = layoutManager.itemCount
                        pastVisibleItems = layoutManager.findFirstVisibleItemPosition()
                        if (visibleItemCount + pastVisibleItems >= totalItemCount)
                            doRequest()
                    }
                }
            }
        })
    }

    override fun refreshData() {
        if (!requestInProgress)
            restartPaging()
    }

    protected open fun restartPaging(loadData: Boolean = true) {
        resetFlags()
        resetData()

        if (loadData)
            doRequest()
    }

    private fun resetFlags() {
        requestInProgress = false
        extraDataAvailable = true
        loadMore = true
        pageNumber = 1
    }

    private fun resetData() {
        arrayList = ArrayList()
        basePaginationAdapter.updateDataSet(arrayList)
    }

    protected abstract fun noData()
    abstract fun loadEmptyPlaceholder()

}