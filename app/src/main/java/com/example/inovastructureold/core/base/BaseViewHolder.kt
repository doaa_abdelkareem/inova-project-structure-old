package com.example.inovastructureold.core.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.inovastructureold.core.enums.ItemClickStatus
import io.reactivex.rxjava3.subjects.PublishSubject

abstract class BaseViewHolder<out T>(view: View) : RecyclerView.ViewHolder(view) {
    val onItemClickListener: PublishSubject<Pair<@UnsafeVariance T, ItemClickStatus>> =
        PublishSubject.create()

    abstract fun bind(item: @UnsafeVariance T)
}