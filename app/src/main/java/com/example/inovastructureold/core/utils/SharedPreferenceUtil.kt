package com.example.inovastructureold.core.utils

import android.content.Context
import android.content.SharedPreferences
import com.example.inovastructureold.MoviesApp

object SharedPreferenceUtil {

    private val sharedPreferences: SharedPreferences by lazy {
        MoviesApp.context
            .getSharedPreferences(Constants.App_PREFERENCES, Context.MODE_PRIVATE)
    }

    private val editor: SharedPreferences.Editor by lazy {
        sharedPreferences.edit()
    }
}