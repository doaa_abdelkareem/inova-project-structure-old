package com.example.inovastructureold.core.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

object FragmentManagerUtil {
    fun addFragment(
        fragmentManager: FragmentManager,
        fragmentToAdd: Fragment,
        containerId: Int,
        keepInBackStack: Boolean = true,
        backStackName: String? = null
    ) =
        fragmentManager.doTransaction({ add(containerId, fragmentToAdd) }, keepInBackStack, backStackName)

    fun replaceFragment(
        fragmentManager: FragmentManager,
        fragmentToReplace: Fragment,
        containerId: Int,
        keepInBackStack: Boolean = true,
        backStackName: String? = null
    ) =
        fragmentManager.doTransaction(
            { replace(containerId, fragmentToReplace) },
            keepInBackStack,
            backStackName
        )

    private inline fun FragmentManager.doTransaction(
        action: FragmentTransaction.() -> Unit = {},
        keepInBackStack: Boolean = true,
        backStackName: String? = null
    ) {
        val fragmentTransaction = beginTransaction()
        fragmentTransaction.action()
        if (keepInBackStack)
            fragmentTransaction.addToBackStack(backStackName)
        fragmentTransaction.commit()
    }
}