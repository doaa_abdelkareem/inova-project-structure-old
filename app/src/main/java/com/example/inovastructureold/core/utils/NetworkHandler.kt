package com.example.inovastructureold.core.utils

import android.content.Context
import android.net.ConnectivityManager
import com.example.inovastructureold.MoviesApp

interface NetworkHandler {

    fun isOnline(): Boolean
}

class NetworkHandlerImpl : NetworkHandler {
    /* TODO try to update deprecated code
        check this link for this part https://stackoverflow.com/questions/57277759/getactivenetworkinfo-is-deprecated-in-api-29
     */
    override fun isOnline(): Boolean {
        return try {
            val cm = MoviesApp.context.getSystemService(Context.CONNECTIVITY_SERVICE)
                    as ConnectivityManager
            cm.activeNetworkInfo?.isConnectedOrConnecting ?: false
        } catch (e: Exception) {
            false
        }
    }

}