package com.example.inovastructureold.core.utils

import android.view.View
import com.example.inovastructureold.R
import com.google.android.material.snackbar.Snackbar

// TODO you could make these methods extension instead of static. that way is cleaner
object ErrorDisplay {
    private fun create(view: View, error: Throwable, retry: (() -> Unit)? = null): Snackbar =
        Snackbar.make(
            view,
            error.message!!,
            if (retry != null) Snackbar.LENGTH_INDEFINITE else Snackbar.LENGTH_LONG
        ).apply {
            if (retry != null) {
                this.setAction(view.context.getText(R.string.retry)) { retry.invoke() }
            }
        }

    fun show(view: View, error: Throwable, retry: (() -> Unit)? = null) =
        create(view, error, retry).show()
}
