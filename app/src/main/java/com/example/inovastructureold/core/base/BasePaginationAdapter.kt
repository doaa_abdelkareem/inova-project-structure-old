package com.example.inovastructureold.core.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.example.inovastructureold.R


abstract class BasePaginationAdapter<out T>(@LayoutRes val viewLayout: Int) :
    BaseAdapter<@UnsafeVariance T>() {

    companion object {
        const val TYPE_ITEM = 0
        const val TYPE_LOADING_FOOTER = 1
    }

    protected var isLoadingProgressBarVisible = false
    lateinit var parent: ViewGroup

    // TODO example for factory suggestion
    private val view get() = LayoutInflater.from(parent.context).inflate(viewLayout, parent, false)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == TYPE_LOADING_FOOTER)
            getLoadingViewHolder(parent)
        else
            makeViewHolder(view).apply {
                onItemClickListener.subscribe(
                    this@BasePaginationAdapter.onItemClickListener
                )
            }
    }

    abstract fun makeViewHolder(view: View): BaseViewHolder<T>

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == TYPE_ITEM)
            super.onBindViewHolder(holder, position)
        else if (getItemViewType(position) == TYPE_LOADING_FOOTER)
            (holder as? LoadingViewHolder)?.setProgressBarVisibility(isLoadingProgressBarVisible)
    }

    override fun getItemCount(): Int =
        if (isLoadingProgressBarVisible) arrayList.size + 1 else arrayList.size

    override fun getItemViewType(position: Int) =
        if (arrayList.size - 1 >= position) TYPE_ITEM else TYPE_LOADING_FOOTER

    fun setLoadingProgressBarVisibility(isVisible: Boolean) {
        isLoadingProgressBarVisible = isVisible
        notifyItemChanged(arrayList.size)
    }

    fun getLoadingViewHolder(parent: ViewGroup) =
        LoadingViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.loader, parent, false)
        )

    open fun appendDataSet(newList: ArrayList<@UnsafeVariance T>?) {
        newList?.let {
            arrayList.addAll(newList)
            notifyDataSetChanged()
        }
    }

}