package com.example.inovastructureold.core.enums

enum class ItemClickStatus {
    OPEN, EDIT
}