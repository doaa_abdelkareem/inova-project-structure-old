package com.example.inovastructureold.core.base

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.inovastructureold.R
import com.example.inovastructureold.core.utils.Constants
import com.example.inovastructureold.core.utils.ErrorDisplay
import com.example.inovastructureold.core.utils.KeyboardUtil
import com.google.android.material.snackbar.Snackbar
import io.reactivex.rxjava3.core.SingleObserver
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import retrofit2.HttpException

abstract class BaseFragment<T, VM : ViewModel>(layoutId: Int) : Fragment(layoutId) {

    protected abstract val viewModel: VM
    protected lateinit var mContext: Context
    protected var swipeRefreshLayout: SwipeRefreshLayout? = null
    protected var bag = CompositeDisposable()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews(view)
        attachListeners()
    }


    override fun onDestroyView() {
        super.onDestroyView()

        view?.let {
            KeyboardUtil.hide(it)
        }
        bag.clear()
    }

    override fun onDestroy() {
        super.onDestroy()

        bag.dispose()
    }

    protected open fun initViews(parentView: View?) {}

    protected fun initSwipeToRefreshListener(view: SwipeRefreshLayout) {
        swipeRefreshLayout = view
        swipeRefreshLayout?.setOnRefreshListener {
            refreshData()
        }
    }

    protected open fun attachListeners() {}

    open fun refreshData() {

    }

    protected open fun doRequest(type: String = Constants.REQUEST_SINGLE) {
        view?.let {
            KeyboardUtil.hide(it)
        }
    }

    fun getObserver(onRequestComplete: () -> Unit = { onRequestCompleted() }): SingleObserver<T> {
        return object : SingleObserver<T> {
            override fun onSubscribe(d: Disposable) {
                bag.add(d)
            }

            override fun onSuccess(t: T) {
                onRequestComplete()
                onRequestSucceeded(t)
            }

            override fun onError(e: Throwable) {
                onRequestComplete()
                onRequestFailed(e)
            }
        }
    }

    open fun onRequestCompleted() {}

    open fun onRequestSucceeded(item: T) {}

    protected open fun onRequestFailed(t: Throwable) {

        if (t is HttpException) {
            view?.let {
                Snackbar.make(
                    it,
                    getString(R.string.something_went_wrong),
                    Snackbar.LENGTH_LONG
                ).show()

            }
        } else {
            view?.let { ErrorDisplay.show(it, t) }
        }

    }

    //abstract fun retryRequest()
}