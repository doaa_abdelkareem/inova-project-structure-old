package com.example.inovastructureold.core.base

import androidx.recyclerview.widget.RecyclerView
import com.example.inovastructureold.core.enums.ItemClickStatus
import io.reactivex.rxjava3.subjects.PublishSubject


abstract class BaseAdapter<out T> :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var arrayList = arrayListOf<@UnsafeVariance T>()
    var onItemClickListener: PublishSubject<Pair<@UnsafeVariance T, ItemClickStatus>> =
        PublishSubject.create()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? BaseViewHolder<@UnsafeVariance T>)?.bind(arrayList[position])
    }

    override fun getItemCount(): Int = arrayList.size

    open fun updateDataSet(newList: ArrayList<@UnsafeVariance T>?) {
        newList?.let {
            arrayList = it
            notifyDataSetChanged()
        }
    }

    fun addItem(t: @UnsafeVariance T) {
        arrayList.add(t)
        notifyItemInserted(arrayList.lastIndex)
    }

    fun removeItem(t: @UnsafeVariance T) {
        val position = arrayList.indexOf(t)
        if (position != -1) {
            arrayList.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, arrayList.size)
        }
    }

    fun clear() {
        arrayList.clear()
        notifyDataSetChanged()
    }
}