package com.example.inovastructureold.core.base

import com.example.inovastructureold.core.utils.NetworkHandler
import io.reactivex.rxjava3.core.Single

abstract class BaseRepo(
    private val networkHandler: NetworkHandler
) {
    fun <T> fetch(
        get: () -> Single<List<T>>, getCached: () -> Single<List<T>>,
        cache: (items: List<T>) -> Unit
    ): Single<List<T>> {
        if (networkHandler.isOnline()) {
            return get()
                .map {
                    cache(it)
                    it
                }
        }
        return getCached()
    }
}