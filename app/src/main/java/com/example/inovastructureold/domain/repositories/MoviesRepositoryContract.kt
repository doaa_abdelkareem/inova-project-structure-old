package com.example.inovastructureold.domain.repositories

import com.example.inovastructureold.models.Movie
import io.reactivex.rxjava3.core.Single

interface MoviesRepository {
    fun getPopularMovies(page: Int): Single<List<Movie>>
}

interface MoviesRemoteDataSource: MoviesRepository

interface MoviesLocalDataSource: MoviesRepository {
    fun cachePopularMovies(movies: List<Movie>)
}