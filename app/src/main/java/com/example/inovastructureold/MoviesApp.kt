package com.example.inovastructureold

import android.app.Application
import com.example.inovastructureold.data.manualdi.AppContainer
import timber.log.Timber


class MoviesApp : Application() {

    override fun onCreate() {
        super.onCreate()

        context = this

        appContainer = AppContainer()
        initTimber()

    }


    private fun initTimber() {
        Timber.plant(Timber.DebugTree())
    }

    companion object {

        // TODO better syntax suggestion for kotlin
        private lateinit var mContext: MoviesApp
        var context: MoviesApp
        get() = mContext
        set(value) { mContext = value}
        lateinit var appContainer: AppContainer
    }

}